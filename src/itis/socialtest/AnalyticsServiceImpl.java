package itis.socialtest;

import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AnalyticsServiceImpl implements AnalyticsService {


    @Override
    public List<Post> findPostsByDate(List<Post> posts, String date) {
        String dateDay = date.split("T")[0];
        return posts.stream()
                .filter(post -> post.getDate().substring(0, 10).equals(dateDay))
                .collect(Collectors.toList());
    }

    //TODO
    @Override
    public String findMostPopularAuthorNickname(List<Post> posts) {
        Map<Author, List<Post>> collect = posts.stream()
                .collect(Collectors.groupingBy(Post::getAuthor));

        return null;
    }

    @Override
    public Boolean checkPostsThatContainsSearchString(List<Post> posts, String searchString) {
        return posts.stream().anyMatch(post -> post.getContent().contains(searchString));
    }

    @Override
    public List<Post> findAllPostsByAuthorNickname(List<Post> posts, String nick){
        return posts.stream()
                .filter(post -> post.getAuthor().getNickname().equals(nick))
                .collect(Collectors.toList());
    }
}
