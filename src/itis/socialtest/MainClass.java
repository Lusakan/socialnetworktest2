package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;
import itis.socialtest.AnalyticsServiceImpl;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.*
 * 2. Выведите все посты за сегодняшнюю дату*
 * 3. Выведите все посты автора с ником "varlamov" *
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия" *
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        new MainClass().run("C:\\Users\\PC-1\\Desktop\\socialnetworktest2\\src\\itis\\socialtest\\resources\\PostDatabase.csv",
                "C:\\Users\\PC-1\\Desktop\\socialnetworktest2\\src\\itis\\socialtest\\resources\\Authors.csv");
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        AnalyticsServiceImpl asi = new AnalyticsServiceImpl();
        List<Author> authorsList = parseAuthors(new File(authorsSourcePath));
        List<Post> postsList = parsePosts(new File(postsSourcePath), authorsList);

        //РАБОТАЕТ
        printPosts(postsList);
        findPostsByDate(postsList, "17.04.2021T10:00");
        printAuthorsPosts(postsList, "varlamov");
        System.out.println(
                asi.checkPostsThatContainsSearchString(postsList, "Россия") ? "Содержит" : "Не содержит");
        //
    }

    List<Post> parsePosts(File file, List<Author> authors) {
        List<Post> postList = new ArrayList<>();
        try {
            FileReader postsFileReader = new FileReader(file);
            BufferedReader postsBR = new BufferedReader(postsFileReader);

            String post = postsBR.readLine();
            while (post != null) {
                String[] postsArr = post.split(", ", 4);
                postList.add(new Post(postsArr[2], postsArr[3],
                        Long.parseLong(postsArr[1]), findAuthorByID(authors, Long.parseLong(postsArr[0]))));
                post = postsBR.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return postList;
    }

    List<Author> parseAuthors(File file) {
        List<Author> authors = new ArrayList<>();
        try {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);

            String authorString = br.readLine();
            while (authorString != null) {
                String[] a = authorString.split(", ");
                authors.add(new Author(Long.parseLong(a[0]), a[1], a[2]));
                authorString = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return authors;
    }

    Author findAuthorByID(List<Author> authors, long id) {
        return authors.stream().filter(author -> author.getId() == id).findFirst().orElse(null);
    }

    private void printPosts(List<Post> postsList) {
        for (Post post : postsList) {
            System.out.println(post);
        }
    }

    private void printAuthorsPosts(List<Post> posts, String nickname) {
        for (Post post : analyticsService.findAllPostsByAuthorNickname(posts, nickname)) {
            System.out.println(post);
        }
    }

    private void findPostsByDate(List<Post> posts, String date) {
        for (Post post : analyticsService.findPostsByDate(posts, date)) {
            System.out.println(post);
        }
    }

}
